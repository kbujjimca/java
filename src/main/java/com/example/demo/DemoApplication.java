package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		// This is base boot startup class
		// Second comment
		SpringApplication.run(DemoApplication.class, args);
	}
}
