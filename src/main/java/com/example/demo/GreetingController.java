package com.example.demo;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class GreetingController {

	@RequestMapping("/")
    public String index() {
        return "This is Basic spring boot example to create docker images and deploy the code on kubernates"+" it will cover devops basic concepts designed by Bujji Kancharla from Calgary, Canada";
    }
}